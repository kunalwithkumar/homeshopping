import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { Router } from '@angular/router';
import { Admin } from './../admin';
import { User } from './../user';
import { Product } from './../product';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
})
export class AdminComponent implements OnInit {
  constructor(private adminService: AdminService, private router: Router) {}

  ngOnInit(): void {}

  admin = new Admin();
  user = new User();
  product = new Product();

  users: User[] | undefined;
  products: Product[] | undefined;

  logout(adminId: number) {
    this.adminService.logout(adminId).subscribe((data) => {
      console.log(data);
    });
  }

  getUsers() {
    this.adminService.getUsers().subscribe((data) => {
      console.log(data);
    });
  }

  removeUser(userId: number) {
    this.adminService.removeUser(userId).subscribe((data) => {
      console.log(data);
    });
  }
  getProducts() {
    this.adminService.getProducts().subscribe((data) => {
      console.log(data);
    });
  }

  addProduct(product: Product) {
    this.adminService.addProduct(product).subscribe((data) => {
      console.log(data);
    });
  }

  updateProduct(product: Product) {
    this.adminService.updateProduct(product).subscribe((data) => {
      console.log(data);
    });
  }

  removeProduct(productId: number) {
    this.adminService.removeProduct(productId).subscribe((data) => {
      console.log(data);
    });
  }

  bulkUpload(abc: any) {
    this.adminService.bulkUpload(abc).subscribe((data) => {
      console.log(data);
    });
  }

  getReport() {
    this.adminService.getReport().subscribe((data) => {
      console.log(data);
    });
  }

  getStock() {
    this.adminService.getStock().subscribe((data) => {
      console.log(data);
    });
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Admin } from './admin';
import { Product } from './product';
import { User } from './user';

@Injectable({
  providedIn: 'root',
})
export class AdminService {
  constructor(private http: HttpClient) {}

  url: string = 'http://localhost:9191/admin/';

  adminLogin(loginDTO: any): Observable<Admin> {
    return this.http.post<Admin>(this.url + 'login', loginDTO);
  }

  addAdmin(admin: Admin) {
    return this.http.post(this.url + 'register', admin);
  }

  logout(adminId: number) {
    return this.http.get(this.url + 'logout/' + adminId);
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.url + 'getUsers');
  }

  removeUser(userId: number) {
    return this.http.delete(this.url + 'removeUser/' + userId);
  }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.url + 'getProducts');
  }

  addProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(this.url + 'addProduct', product);
  }

  updateProduct(product: Product): Observable<Product> {
    return this.http.put<Product>(this.url + 'updateProduct', product);
  }

  removeProduct(productId: number) {
    return this.http.delete(this.url + 'removeProduct/' + productId);
  }

  bulkUpload(abc: any): Observable<Product[]> {
    return this.http.post<Product[]>(this.url + 'bulkUpload', abc);
  }

  getReport(): Observable<Product[]> {
    return this.http.get<Product[]>(this.url + 'getReport');
  }

  getStock(): Observable<Product[]> {
    return this.http.get<Product[]>(this.url + 'getStock');
  }
}

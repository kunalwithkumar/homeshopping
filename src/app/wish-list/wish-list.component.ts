import { Component, OnInit } from '@angular/core';
import { UserService } from './../user.service';
import { Router } from '@angular/router';
import { User } from '../user';
import { WishList } from '../wish-list';

@Component({
  selector: 'app-wish-list',
  templateUrl: './wish-list.component.html',
  styleUrls: ['./wish-list.component.css'],
})
export class WishListComponent implements OnInit {
  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {}

  user = new User();
  wishlist: WishList[] | undefined;

  addToWishlist(cartDTO: any) {
    this.userService.addToWishlist(cartDTO).subscribe((data) => {
      console.log(data);
    });
  }
  getWishList(userId: number) {
    this.userService.getWishList(userId).subscribe((data) => {
      console.log(data);
    });
  }
  removeFromWishList(wishListDTO: any) {
    this.userService.removeFromWishList(wishListDTO).subscribe((data) => {
      console.log(data);
    });
  }
}

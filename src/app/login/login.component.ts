import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Admin } from '../admin';
import { AdminService } from '../admin.service';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(
    private userService: UserService,
    private adminService: AdminService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  user = new User();
  admin = new Admin();

  login(loginDTO: any) {
    this.userService.login(loginDTO).subscribe((data) => {
      console.log(data);
    });
  }

  adminLogin(loginDTO: any) {
    this.adminService.adminLogin(loginDTO).subscribe((data) => {
      console.log(data);
    });
  }
}

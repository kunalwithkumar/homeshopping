import { Component, OnInit } from '@angular/core';
import { UserService } from './../user.service';
import { AdminService } from './../admin.service';
import { Router } from '@angular/router';
import { User } from '../user';
import { Admin } from '../admin';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  constructor(
    private userService: UserService,
    private adminService: AdminService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  user = new User();
  admin = new Admin();

  register(user: User) {
    this.userService.register(user).subscribe((data) => {
      console.log(data);
    });
  }
  addAdmin(admin: Admin) {
    this.adminService.addAdmin(admin).subscribe((data) => {
      console.log(data);
    });
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './user';
import { Observable } from 'rxjs';
import { Product } from './product';
import { Cart } from './cart';
import { WishList } from './wish-list';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  url: string = 'http://localhost:9191/user/';

  register(user: User): Observable<User> {
    return this.http.post<User>(this.url + 'register', user);
  }

  login(loginDTO: any): Observable<User> {
    return this.http.post<User>(this.url + 'login', loginDTO);
  }

  logout(userId: number) {
    return this.http.get(this.url + 'logout/' + userId);
  }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.url + 'getProducts');
  }

  getSortedProducts(sorted: string): Observable<Product[]> {
    return this.http.get<Product[]>(this.url + 'getProduct/' + sorted);
  }

  getCategoryProducts(category: string): Observable<Product[]> {
    return this.http.get<Product[]>(this.url + 'getProduct/' + category);
  }

  addToCart(cartDTO: any): Observable<Cart[]> {
    return this.http.post<Cart[]>(this.url + 'addToCart', cartDTO);
  }

  addToWishlist(cartDTO: any): Observable<WishList[]> {
    return this.http.post<WishList[]>(this.url + 'addToWishlist', cartDTO);
  }

  getCart(userId: number): Observable<Cart[]> {
    return this.http.get<Cart[]>(this.url + 'getCart/' + userId);
  }

  getWishList(userId: number): Observable<WishList[]> {
    return this.http.get<WishList[]>(this.url + 'getWishList/' + userId);
  }

  removeFromCart(cartDTO: any) {
    return this.http.delete(this.url + 'removeFromCart', cartDTO);
  }

  removeFromWishList(wishListDTO: any) {
    return this.http.delete(this.url + 'removeFromWishList', wishListDTO);
  }
}

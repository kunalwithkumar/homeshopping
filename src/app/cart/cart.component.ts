import { Component, OnInit } from '@angular/core';
import { Cart } from '../cart';
import { User } from '../user';
import { UserService } from './../user.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {
  constructor(private userService: UserService) {}

  ngOnInit(): void {}
  user = new User();
  cart: Cart[] | undefined;

  addToCart(cartDTO: any) {
    this.userService.addToCart(cartDTO).subscribe((data) => {
      console.log(data);
    });
  }

  getCart(userId: number) {
    this.userService.getCart(userId).subscribe((data) => {
      console.log(data);
    });
  }

  removeFromCart(cartDTO: any) {
    this.userService.removeFromCart(cartDTO).subscribe((data) => {
      console.log(data);
    });
  }
}

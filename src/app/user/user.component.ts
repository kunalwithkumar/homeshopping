import { Component, OnInit } from '@angular/core';
import { UserService } from './../user.service';
import { Router } from '@angular/router';
import { User } from '../user';
import { Product } from './../product';
import { Cart } from '../cart';
import { WishList } from '../wish-list';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {}

  user = new User();
  product: Product[] | undefined;

  cart: Cart[] | undefined;
  wishlist: WishList[] | undefined;

  logout(userId: number) {
    this.userService.logout(userId).subscribe((data) => {
      console.log(data);
    });
  }

  getProducts() {
    this.userService.getProducts().subscribe((data) => {
      console.log(data);
    });
  }

  getSortedProducts(sorted: string) {
    this.userService.getSortedProducts(sorted).subscribe((data) => {
      console.log(data);
    });
  }

  getCategoryProducts(category: string) {
    this.userService.getCategoryProducts(category).subscribe((data) => {
      console.log(data);
    });
  }
}

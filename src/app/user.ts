export class User {
  id: number = 0;
  email: string = '';
  userName: string = '';
  password: string = '';
  phoneNumber: string = '';
  isActive: boolean = true;
  address: string = '';
}

export class Cart {
  id: number = 0;
  product: any;
  user: any;
  quantity: number = 0;
}

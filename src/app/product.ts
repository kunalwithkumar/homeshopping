export class Product {
  id: number = 0;
  productName: string = '';
  price: string = '';
  category: string = '';
}

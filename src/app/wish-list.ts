export class WishList {
  id: number = 0;
  product: any;
  quantity: number = 0;
  user: any;
}
